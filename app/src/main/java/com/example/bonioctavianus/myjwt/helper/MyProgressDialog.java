package com.example.bonioctavianus.myjwt.helper;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by bonioctavianus on 10/4/16.
 */

public class MyProgressDialog {
    public static final String TAG = MyProgressDialog.class.getSimpleName();

    private ProgressDialog pDialog;

    public MyProgressDialog(Context context, String message) {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(message);
        pDialog.setCancelable(false);
    }

    public void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    public void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}