package com.example.bonioctavianus.myjwt.network;

import com.example.bonioctavianus.myjwt.configuration.Configuration;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by bonioctavianus on 10/4/16.
 */

public class MyRetrofit {

    public static Retrofit getRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Configuration.BACKEND_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient())
                .build();

        return retrofit;
    }
}
