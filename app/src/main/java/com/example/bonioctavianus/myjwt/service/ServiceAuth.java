package com.example.bonioctavianus.myjwt.service;

import com.example.bonioctavianus.myjwt.response.ResponseAuth;

import rx.subjects.PublishSubject;

/**
 * Created by bonioctavianus on 10/4/16.
 */

public interface ServiceAuth {

    PublishSubject<ResponseAuth> getSubjectAuth();

    void requestLogin(String email, String password);

    void postMessage(String message);
}
