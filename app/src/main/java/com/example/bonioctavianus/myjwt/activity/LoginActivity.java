package com.example.bonioctavianus.myjwt.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.example.bonioctavianus.myjwt.R;
import com.example.bonioctavianus.myjwt.helper.MyProgressDialog;
import com.example.bonioctavianus.myjwt.response.ResponseAuth;
import com.example.bonioctavianus.myjwt.service.ServiceAuth;
import com.example.bonioctavianus.myjwt.service.impl.MyServiceAuth;

import rx.Subscriber;
import rx.Subscription;

public class LoginActivity extends AppCompatActivity {
    public static final String TAG = LoginActivity.class.getSimpleName();

    private Subscription subAuth;
    private ServiceAuth serviceAuth;

    private EditText autotvEmail;
    private EditText etPassword;
    private Button btnSignIn;

    private MyProgressDialog pDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        serviceAuth = new MyServiceAuth();
        pDialog = new MyProgressDialog(this, getString(R.string.login_msg_loading));

        initViews();
        subscribeForAuthResponse();
    }

    private void initViews() {
        autotvEmail = (EditText) findViewById(R.id.autotv_email);
        etPassword = (EditText) findViewById(R.id.et_password);
        btnSignIn = (Button) findViewById(R.id.btn_sign_in);

        btnSignIn.setOnClickListener(view -> attemptLogin());
        //btnSignIn.setOnClickListener((view) -> serviceAuth.postMessage("Hello There !"));
    }

    private void attemptLogin() {
        autotvEmail.setError(null);
        etPassword.setError(null);

        String email = autotvEmail.getText().toString();
        String password = etPassword.getText().toString();
        boolean result;

        if (TextUtils.isEmpty(email) && TextUtils.isEmpty(password)) {
            autotvEmail.setError(getString(R.string.login_error_field_required));
            etPassword.setError(getString(R.string.login_error_field_required));
            autotvEmail.requestFocus();
            result = false;

        } else {
            result = checkForValidEmail(email) && checkForValidPassword(password);
        }

        if (result) {
            pDialog.showpDialog();
            serviceAuth.requestLogin(email, password);
        }
    }

    private boolean checkForValidEmail(String email) {
        boolean result = true;

        if (TextUtils.isEmpty(email)) {
            autotvEmail.setError(getString(R.string.login_error_field_required));
            result = false;

        } else if (!isValidEmail(email)) {
            autotvEmail.setError(getString(R.string.login_error_invalid_email));
            result = false;
        }

        if (!result) {
            autotvEmail.requestFocus();
        }

        return result;
    }

    private boolean checkForValidPassword(String password) {
        boolean result = true;

        if (TextUtils.isEmpty(password)) {
            etPassword.setError(getString(R.string.login_error_field_required));
            result = false;

        } else if (!isValidPassword(password)) {
            etPassword.setError(getString(R.string.login_error_invalid_password));
            result = false;
        }

        if (!result) {
            etPassword.requestFocus();
        }

        return result;
    }

    private boolean isValidEmail(String email) {

        return email.contains("@");
    }

    private boolean isValidPassword(String password) {

        return password.length() > 5;
    }

    private void subscribeForAuthResponse() {
        subAuth = serviceAuth.getSubjectAuth().subscribe(new Subscriber<ResponseAuth>() {
            @Override
            public void onCompleted() {
                Log.d(TAG, "on completed response auth called..");
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "on error response auth called..");
            }

            @Override
            public void onNext(ResponseAuth responseAuth) {
                pDialog.hidepDialog();
                Log.d(TAG, "on next response auth called..");
                showNotifyDialog(responseAuth);
            }
        });
    }

    private void showNotifyDialog(ResponseAuth responseAuth) {
        String message;

        if (responseAuth != null) {
            message = responseAuth.error == null ?
                    getString(R.string.login_msg_auth_success) : responseAuth.error;

        } else {
            message = getString(R.string.login_msg_auth_error);
        }

        new AlertDialog.Builder(this)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, null).create().show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (subAuth != null && subAuth.isUnsubscribed()) {
            subAuth.unsubscribe();
        }
    }
}
