package com.example.bonioctavianus.myjwt.network.endpoint;

import com.example.bonioctavianus.myjwt.response.ResponseAuth;
import com.example.bonioctavianus.myjwt.response.ResponseMessage;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by bonioctavianus on 10/4/16.
 */

public interface AuthEndPoint {

    @POST("login")
    @FormUrlEncoded
    Call<ResponseAuth> postLogin(@Field("email") String email, @Field("password") String password);

    @POST("hello")
    @FormUrlEncoded
    Call<ResponseMessage> postMessage(@Header("Authorization") String token, @Field("message") String message);

}
