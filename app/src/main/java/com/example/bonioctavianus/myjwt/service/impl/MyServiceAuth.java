package com.example.bonioctavianus.myjwt.service.impl;

import android.util.Log;

import com.example.bonioctavianus.myjwt.network.MyRetrofit;
import com.example.bonioctavianus.myjwt.network.endpoint.AuthEndPoint;
import com.example.bonioctavianus.myjwt.response.ResponseAuth;
import com.example.bonioctavianus.myjwt.response.ResponseMessage;
import com.example.bonioctavianus.myjwt.service.ServiceAuth;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import rx.subjects.PublishSubject;

/**
 * Created by bonioctavianus on 10/4/16.
 */

public class MyServiceAuth implements ServiceAuth {
    public static final String TAG = MyServiceAuth.class.getSimpleName();

    private PublishSubject<ResponseAuth> subjectAuth;

    public MyServiceAuth() {
        subjectAuth = PublishSubject.create();
    }

    @Override
    public PublishSubject<ResponseAuth> getSubjectAuth() {

        return subjectAuth;
    }

    @Override
    public void requestLogin(String email, String password) {
        Retrofit retrofit = MyRetrofit.getRetrofit();
        AuthEndPoint endPoint = retrofit.create(AuthEndPoint.class);
        Call<ResponseAuth> call = endPoint.postLogin(email, password);

        call.enqueue(new Callback<ResponseAuth>() {
            @Override
            public void onResponse(Call<ResponseAuth> call, Response<ResponseAuth> response) {
                Log.d(TAG, "on response login called..");
                subjectAuth.onNext(response.body());
            }

            @Override
            public void onFailure(Call<ResponseAuth> call, Throwable t) {
                Log.e(TAG, "on failure login called..");
                t.printStackTrace();
                subjectAuth.onNext(null);
            }
        });
    }

    @Override
    public void postMessage(String message) {
        String token = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwIiwiaWF0IjoxNDc1NTU4MzI2LCJpc3MiOiJNeSBDb21wYW55IiwiYXVkIjoiYm9uaUBhcHBzLWZvdW5kcnkuY29tIiwiZXhwIjoxNDc1NTU4MzM2fQ.jR-tAqjg04OWU1mYxsDnXhkaDRmblWv2VuOl2WQRcRw";

        Retrofit retrofit = MyRetrofit.getRetrofit();
        AuthEndPoint endPoint = retrofit.create(AuthEndPoint.class);
        Call<ResponseMessage> call = endPoint.postMessage(token, message);

        call.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                Log.d(TAG, "on response call message..");
                Log.d(TAG, "message : " + response.body().message);
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "on failure call message..");
            }
        });
    }
}
