package com.example.bonioctavianus.myjwt.response;

/**
 * Created by bonioctavianus on 10/4/16.
 */

public class ResponseAuth {

    public String token;
    public String error;
}
