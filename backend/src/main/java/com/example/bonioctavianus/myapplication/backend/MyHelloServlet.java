package com.example.bonioctavianus.myapplication.backend;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by bonioctavianus on 10/4/16.
 */

public class MyHelloServlet extends HttpServlet {

    private String exampleToken = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwIiwiaWF0IjoxNDc1NTU4MzI2LCJpc3MiOiJNeSBDb21wYW55IiwiYXVkIjoiYm9uaUBhcHBzLWZvdW5kcnkuY29tIiwiZXhwIjoxNDc1NTU4MzM2fQ.jR-tAqjg04OWU1mYxsDnXhkaDRmblWv2VuOl2WQRcRw";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String token = req.getHeader("authorization");

        String response;

        if (token != null && token.equals(exampleToken)) {
            resp.setStatus(HttpServletResponse.SC_OK);
            String message = req.getParameter("message");
            response = String.format("{\"message\":\"%s\"}", message);
        } else {
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response = String.format("{\"message\":\"%s\"}", "Unauthorized");
        }

        resp.setContentType("application/json");
        resp.getWriter().print(response);
    }
}
