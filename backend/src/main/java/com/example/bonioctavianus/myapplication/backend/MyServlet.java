/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Servlet Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloWorld
*/

package com.example.bonioctavianus.myapplication.backend;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class MyServlet extends HttpServlet {

    private static final String DUMMY_VALID_USER = "boni@apps-foundry.com:admin12345";

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/plain");
        resp.sendError(HttpServletResponse.SC_FORBIDDEN, "No GET Access");
        //resp.getWriter().println("Please use the form to POST to this url");
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String response;

        resp.setContentType("application/json");

        if (email == null) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response = String.format("{\"error:\" : \"%s\"}", "Email Address is required");

        } else if (password == null) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response = String.format("{\"error\":\"%s\"}", "Password is required");

        } else if ((email + ":" + password).equals(DUMMY_VALID_USER)) {
            String token = generateJwt("0", "My Company", email, 10000);
            response = String.format("{\"token\":\"%s\"}", token);

        } else {
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response = String.format("{\"error\":\"%s\"}", "Email Address or Password is incorrect");
        }

        resp.getWriter().print(response);
    }

    private String generateJwt(String id, String issuer, String audience, long timeMilis) {
        Date currentDate = new Date(System.currentTimeMillis());
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("My Secret");

        JwtBuilder jwtBuilder = Jwts.builder()
                .setId(id)
                .setIssuedAt(currentDate)
                .setIssuer(issuer)
                .setAudience(audience)
                .signWith(SignatureAlgorithm.HS256, apiKeySecretBytes);

        if (timeMilis >= 0) {
            long expiredTimeMilis = System.currentTimeMillis() + timeMilis;
            Date expiredDate = new Date(expiredTimeMilis);
            jwtBuilder.setExpiration(expiredDate);
        }

        return jwtBuilder.compact();
    }
}
